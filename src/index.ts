import World, { Component } from "@rapidajs/recs";
import { WebGLRenderer } from '@rapidajs/three';
import {
  AmbientLight,
  BoxGeometry,
  DirectionalLight,
  Mesh,
  MeshPhongMaterial,
  PerspectiveCamera,
  Scene,
  Vector3,
} from "three";

class SpinningCube extends Component {
  mesh: Mesh;

  scene: Scene;

  construct = (params: { scene: Scene }) => {
    this.scene = params.scene;

    const geometry = new BoxGeometry(10, 10, 10);
    const material = new MeshPhongMaterial();
    this.mesh = new Mesh(geometry, material);
  };

  onUpdate = () => {
    this.mesh.rotation.x += 0.002;
  };

  onInit = () => {
    this.scene.add(this.mesh);
  };
}

const world = new World();

const renderer = new WebGLRenderer();
document.getElementById("rapida").appendChild(renderer.domElement);

const scene = new Scene();

const camera = new PerspectiveCamera();
camera.position.z = 100;

renderer.create.view({
  scene,
  camera,
});

const directionalLight = new DirectionalLight(0xffffff, 1);
directionalLight.position.set(300, 0, 300);
directionalLight.lookAt(new Vector3(0, 0, 0));
scene.add(directionalLight);

const ambientLight = new AmbientLight(0xffffff, 0.5);
scene.add(ambientLight);

const space = world.create.space();

space.create.entity().addComponent(SpinningCube, { scene });

// simple loop
world.init();

let lastCallTime = 0;

const loop = (now: number) => {
  const nowSeconds = now / 1000;
  const elapsed = nowSeconds - lastCallTime;

  world.update(elapsed);
  renderer.render(elapsed);

  lastCallTime = nowSeconds;

  requestAnimationFrame(loop);
};

requestAnimationFrame(loop);
