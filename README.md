# rapida-typescript-parcel

Simple example of how to use rapida with `typescript` and `parcel`.

## Getting Started

```bash
> git clone https://gitlab.com/rapidajs/rapida-typescript-parcel.git

> cd rapida-typescript-parcel

> yarn install

> yarn dev
```
